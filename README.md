# Apple Music .NET MAUI app

## 🎯 Goal

Clone Apple Music app in .NET MAUI

## 📱 Views

| Library | Podcasts list | Podcast view |
| -- | -- | -- |
| <img src="./.github/library.png" width="225" height="500"> | <img src="./.github/library.png" width="225" height="500"> | <img src="./.github/library.png" width="225" height="500"> |

## 👨‍💻 Author 

**BREUIL Yohann**

* GitHub: [@DJYohann](https://github.com/DJYohann)
* LinkedIn: [@BREUIL Yohann](https://www.linkedin.com/in/yohann-breuil-02b18a165/)