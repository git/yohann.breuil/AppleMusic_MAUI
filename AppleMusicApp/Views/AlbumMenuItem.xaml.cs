﻿namespace AppleMusicApp.Views;

using Pages;

public partial class AlbumMenuItem : ContentView
{
	public AlbumMenuItem()
	{
		InitializeComponent();
	}

    void AlbumClicked(System.Object sender, System.EventArgs e)
    {
		Shell.Current.GoToAsync(nameof(AlbumPage));
    }
}
