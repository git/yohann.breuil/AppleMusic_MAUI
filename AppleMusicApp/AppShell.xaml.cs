﻿namespace AppleMusicApp;

using Pages;

public partial class AppShell : Shell
{
	public AppShell()
	{
		InitializeComponent();
		CurrentItem = LibraryTab;

		Routing.RegisterRoute(nameof(AlbumPage), typeof(AlbumPage));
	}
}

