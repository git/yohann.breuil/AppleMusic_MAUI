﻿using System;
namespace AppleMusicApp.Model
{
	public class Track
	{
		public string Name { get; set; }

        public bool IsLiked { get; set; }

        public bool IsDownloaded { get; set; }

        public Track()
		{
		}
	}
}

