﻿using System;
namespace AppleMusicApp.Model
{
	public class Album
	{
		public string Name { get; set; }

		public string CoverImage { get; set; }

		public DateTime ReleaseDate { get; set; }

		public int Duration { get; set; }

		public string RecordLabel { get; set; }

		public List<Track> tracks { get; set; }

        public Album()
		{
		}
	}
}

