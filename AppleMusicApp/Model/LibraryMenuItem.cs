﻿using System;
namespace AppleMusicApp.Model
{
	public class LibraryMenuItem
	{
		public string iconName { get; set; }

		public string text { get; set; }

        public LibraryMenuItem(string iconName, string text)
		{
			this.iconName = iconName;
			this.text = text;
		}
	}
}

